FROM rust:latest

COPY ./webserver/  /usr/local/webserver
WORKDIR /usr/local/webserver
RUN cargo build --release
EXPOSE 7878
CMD cargo run
