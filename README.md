# .gitlab-ci.yml

```
stages:
  - build
  - deploy

variables:
  CI_REGISTRY_IMAGE: "myserver"
  NAMESPACE: "rust-app"
#docker-build:env:
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $DOCKER_HUB_TOKEN
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_USER/$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_USER/$CI_REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
    - changes:
        paths:
          - Dockerfile
          - webserver/
      exists:
        - Dockerfile

deploy:
  stage: deploy
  only: [tags]
  image: dtzar/helm-kubectl
  before_script:
    - kubectl config set-cluster cluster --server="$K8S_SERVER" --insecure-skip-tls-verify=true
    - kubectl config set-credentials gitlab --token=$K8S_TOKEN
    - kubectl config set-context gitlab --cluster=cluster --user=gitlab
    - kubectl config use-context gitlab

  script:
    - helm upgrade app Chart/ --install --recreate-pods
```

# Развернул приложение
```
helm ► helm install app Chart/
NAME: app
LAST DEPLOYED: Fri Apr  7 21:26:04 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```
Проверяем его работу 
https://gitlab.com/AGS-36/rust_webserver/-/tree/main/images/1.png

# Вношу изменения и добавляю tag
https://gitlab.com/AGS-36/rust_webserver/-/tree/main/images/3.png
https://gitlab.com/AGS-36/rust_webserver/-/tree/main/images/5.png

# Проверяю еще раз, сперва делаю push без тега и убеждаюсь, что deploy не выполняется, а после добавляю tag

https://gitlab.com/AGS-36/rust_webserver/-/tree/main/images/4.png

